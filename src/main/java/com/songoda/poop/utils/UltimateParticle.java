package com.songoda.poop.utils;

import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class UltimateParticle {

	private final double offsetX, offsetY, offsetZ;
	private final ConfigurationSection section;
	private final int count, size;
	private Particle particle;
	private final long delay;
	private Color color;

	public UltimateParticle(ConfigurationSection section, String fallback) {
		this.section = section;
		this.offsetX = section.getDouble("offset-x", 0.0);
		this.offsetY = section.getDouble("offset-y", 0.0);
		this.offsetZ = section.getDouble("offset-z", 0.0);
		this.count = section.getInt("amount", 50);
		this.delay = section.getLong("delay", 0);
		if (section.isSet("color"))
			this.color = DyeColor.valueOf(section.getString("color", "RED")).getColor();
		this.size = section.getInt("size", 1);
		try {
			particle = Particle.valueOf(section.getString("type", fallback));
		} catch (Exception e) {
			particle = Particle.EXPLOSION_NORMAL;
		}
	}

	public void playTo(Location location, Player... players) {
		for (Player player : players)
			if (color != null)
				player.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, 0.1, new Particle.DustOptions(color, size));
			else
				player.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, 0.1);
	}

	public void playAt(Location... locations) {
		for (Location location : locations)
			if (color != null)
				location.getWorld().spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, 0.1, new Particle.DustOptions(color, size));
			else
				location.getWorld().spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, 0.1);
	}

	public ConfigurationSection getConfigurationSection() {
		return section;
	}

	public Particle getParticle() {
		return particle;
	}

	public double getOffsetX() {
		return offsetX;
	}

	public double getOffsetY() {
		return offsetY;
	}

	public double getOffsetZ() {
		return offsetZ;
	}

	public long getDelay() {
		return delay;
	}

	public int getCount() {
		return count;
	}

}
