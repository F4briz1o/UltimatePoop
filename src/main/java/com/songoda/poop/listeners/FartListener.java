package com.songoda.poop.listeners;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;

import com.songoda.poop.UltimatePoop;
import com.songoda.poop.events.FartEvent;
import com.songoda.poop.events.PoopEvent;
import com.songoda.poop.utils.ParticlePlayer;
import com.songoda.poop.utils.SoundPlayer;
import com.songoda.poop.utils.Utils;

public class FartListener implements Listener {

	private final Map<Player, Integer> farts = new HashMap<>();
	private final Map<Player, Integer> fps = new HashMap<>();
	private final FileConfiguration configuration;
	private final PluginManager pluginManager;
	private final UltimatePoop instance;

	public FartListener(UltimatePoop instance) {
		this.pluginManager = instance.getServer().getPluginManager();
		this.configuration = instance.getConfig();
		this.instance = instance;
	}

	public int getFartsPerSecond(Player player) {
		return Optional.ofNullable(fps.get(player)).orElse(0);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onFart(PlayerToggleSneakEvent event) {
		if (event.isCancelled())
			return;
		ConfigurationSection section = configuration.getConfigurationSection("farts");
		if (!section.getBoolean("enabled", true))
			return;
		Player player = event.getPlayer();
		FartEvent fart = new FartEvent(player);
		pluginManager.callEvent(fart);
		if (fart.isCancelled())
			return;
		if (!player.hasPermission("ultimatepoop.fart"))
			return;
		Location location = player.getLocation();
		new ParticlePlayer(section.getConfigurationSection("particles")).playAt(location);
		new SoundPlayer(section.getConfigurationSection("sounds")).playAt(location);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onSneak(PlayerToggleSneakEvent event) {
		if (event.isCancelled())
			return;
		count(event.getPlayer());
	}

	@EventHandler
	public void checkFart(FartEvent event) {
		if (event.isCancelled())
			return;
		ConfigurationSection poopSection = configuration.getConfigurationSection("poop");
		int count = Optional.ofNullable(farts.get(event.getPlayer())).orElse(0);
		if (count >= poopSection.getInt("fart-count", 5)) {
			if (poopSection.getBoolean("cancel-fart-if-poop", true))
				event.setCancelled(true);
		}
	}

	public void count(Player player) {
		// FPS
		int perSecond = Optional.ofNullable(fps.get(player)).orElse(0);
		fps.put(player, perSecond+=1);
		instance.getServer().getScheduler().runTaskLaterAsynchronously(instance, () -> {
			int amount = Optional.ofNullable(fps.get(player)).orElse(0);
			if (amount <= 1)
				fps.remove(player);
			else
				fps.put(player, amount - 1);
		}, 20);
		
		int count = Optional.ofNullable(farts.get(player)).orElse(0);
		farts.put(player, count+=1);
		instance.getServer().getScheduler().runTaskLaterAsynchronously(instance, () -> {
			int amount = Optional.ofNullable(farts.get(player)).orElse(0);
			if (amount <= 1)
				farts.remove(player);
			else
				farts.put(player, amount - 1);
		}, configuration.getInt("farts.cooldown", 30));
		ConfigurationSection poopSection = configuration.getConfigurationSection("poop");
		if (count >= poopSection.getInt("fart-count", 5)) {
			PoopEvent poop = new PoopEvent(player);
			pluginManager.callEvent(poop);
			if (poop.isCancelled())
				return;
			if (!player.hasPermission("ultimatepoop.poop"))
				return;
			Material material = Utils.materialAttempt(poopSection.getString("item.material", "COCOA_BEANS"), "COCOA_BEANS");
			Item item = player.getWorld().dropItemNaturally(player.getLocation(), new ItemStack(material));
			if (!poopSection.getBoolean("poop-can-be-picked-up", false))
				item.setPickupDelay(Integer.MAX_VALUE);
			if (poopSection.isSet("item.name")) {
				ItemMeta meta = item.getItemStack().getItemMeta();
				meta.setDisplayName(Utils.color(poopSection.getString("item.name", "&e{player}'s poop").replace("{player}", player.getName())));
			}
			new SoundPlayer(poopSection.getConfigurationSection("sounds")).playAt(player.getLocation());
			new ParticlePlayer(poopSection.getConfigurationSection("particles")).playAt(player.getLocation());
			if (poopSection.getBoolean("despawn.enabled", false))
				instance.getServer().getScheduler().runTaskLater(instance, () -> item.remove(), poopSection.getInt("despawn.despawn-time", 20));
		}
	}

}
