package com.songoda.poop.utils;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.songoda.poop.UltimatePoop;

public class SoundPlayer {

	private final Set<UltimateSound> sounds = new HashSet<>();
	private final UltimatePoop instance;
	private final boolean enabled;
	
	public SoundPlayer(ConfigurationSection section) {
		this.instance = UltimatePoop.getInstance();
		this.enabled = section.getBoolean("enabled", true);
		section = section.getConfigurationSection("sounds");
		for (String node : section.getKeys(false))
			this.sounds.add(new UltimateSound(section.getConfigurationSection(node), "BLOCK_FIRE_EXTINGUISH"));
	}
	
	private List<UltimateSound> getSorted() {
		return sounds.parallelStream()
				.sorted(Comparator.comparing(UltimateSound::getDelay))
				.collect(Collectors.toList());
	}
	
	public void playAt(Location... locations) {
		if (!enabled)
			return;
		if (sounds.isEmpty())
			return;
		for (UltimateSound sound : getSorted()) {
			if (sound.getDelay() <= 0) {
				sound.playAt(locations);
				continue;
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				@Override
				public void run() {
					sound.playAt(locations);
				}
			}, sound.getDelay());
		}
	}
	
	public void playTo(Player... player) {
		if (!enabled)
			return;
		if (sounds.isEmpty())
			return;
		for (UltimateSound sound : getSorted()) {
			if (sound.getDelay() <= 0) {
				sound.playTo(player);
				continue;
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				@Override
				public void run() {
					sound.playTo(player);
				}
			}, sound.getDelay());
		}
	}
	
}
