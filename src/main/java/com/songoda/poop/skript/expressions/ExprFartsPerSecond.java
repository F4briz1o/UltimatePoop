package com.songoda.poop.skript.expressions;

import org.bukkit.entity.Player;
import org.eclipse.jdt.annotation.Nullable;

import com.songoda.poop.UltimatePoop;

import ch.njol.skript.expressions.base.SimplePropertyExpression;

public class ExprFartsPerSecond extends SimplePropertyExpression<Player, Number> {

	static {
		register(ExprFartsPerSecond.class, Number.class, "farts (a|per) second", "players");
	}

	@Override
	public Class<? extends Number> getReturnType() {
		return Number.class;
	}

	@Override
	protected String getPropertyName() {
		return "farts per second";
	}

	@Override
	@Nullable
	public Number convert(Player player) {
		return UltimatePoop.getInstance().getListener().getFartsPerSecond(player);
	}
	
}
