package com.songoda.poop.utils;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class UltimateSound {
	
	private final float pitch, volume;
	private final Sound sound;
	private final int delay;
	
	public UltimateSound(ConfigurationSection section, String fallback) {
		String name = section.getString("sound", "BLOCK_FIRE_EXTINGUISH");
		this.volume = (float) section.getDouble("volume", 1);
		this.pitch = (float) section.getDouble("pitch", 1);
		this.sound = Utils.soundAttempt(name, fallback);
		this.delay = section.getInt("delay", 0);
	}
	
	public int getDelay() {
		return delay;
	}
	
	public Sound getSound() {
		return sound;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public float getVolume() {
		return volume;
	}
	
	public void playTo(Player... players) {
		for (Player player : players)
			player.playSound(player.getLocation(), sound, volume, pitch);
	}
	
	public void playAt(Location... locations) {
		for (Location location : locations)
			location.getWorld().playSound(location, sound, volume, pitch);
	}

}