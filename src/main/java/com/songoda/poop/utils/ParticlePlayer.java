package com.songoda.poop.utils;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.songoda.poop.UltimatePoop;

public class ParticlePlayer {

	private final Set<UltimateParticle> particles = new HashSet<>();
	private final UltimatePoop instance;
	private final boolean enabled;

	public ParticlePlayer(ConfigurationSection section) {
		this.enabled = section.getBoolean("enabled", true);
		this.instance = UltimatePoop.getInstance();
		section = section.getConfigurationSection("particles");
		for (String node : section.getKeys(false))
			this.particles.add(new UltimateParticle(section.getConfigurationSection(node), "EXPLOSION_NORMAL"));
	}
	
	private List<UltimateParticle> getSorted() {
		return particles.parallelStream()
				.sorted(Comparator.comparing(UltimateParticle::getDelay))
				.collect(Collectors.toList());
	}
	
	public void playAt(Location... locations) {
		if (!enabled)
			return;
		if (particles.isEmpty())
			return;
		for (UltimateParticle particle : getSorted()) {
			if (particle.getDelay() <= 0) {
				particle.playAt(locations);
				continue;
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				@Override
				public void run() {
					particle.playAt(locations);
				}
			}, particle.getDelay());
		}
	}
	
	public void playTo(Location location, Player player) {
		if (!enabled)
			return;
		if (particles.isEmpty())
			return;
		for (UltimateParticle particle : getSorted()) {
			if (particle.getDelay() <= 0) {
				particle.playTo(location, player);
				continue;
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				@Override
				public void run() {
					particle.playTo(location, player);
				}
			}, particle.getDelay());
		}
	}

}
