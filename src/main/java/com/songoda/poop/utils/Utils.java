package com.songoda.poop.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;

public class Utils {

	public static EntityType entityAttempt(String attempt, String fallback) {
		EntityType entity = null;
		try {
			entity = EntityType.valueOf(attempt.toUpperCase());
		} catch (Exception e) {
			try {
				entity = EntityType.valueOf(fallback);
			} catch (Exception e1) {}
		}
		if (entity == null)
			entity = EntityType.ARROW;
		return entity;
	}

	public static Material materialAttempt(String attempt, String fallback) {
		Material material = null;
		try {
			material = Material.valueOf(attempt.toUpperCase());
		} catch (Exception e) {
			try {
				material = Material.valueOf(fallback);
			} catch (Exception e1) {}
		}
		if (material == null)
			material = Material.CHEST;
		return material;
	}

	public static Sound soundAttempt(String attempt, String fallback) {
		Sound sound = null;
		try {
			sound = Sound.valueOf(attempt.toUpperCase());
		} catch (Exception e) {
			try {
				sound = Sound.valueOf(fallback);
			} catch (Exception e1) {}
		}
		if (sound == null)
			sound = Sound.ENTITY_PLAYER_LEVELUP;
		return sound;
	}

	public static String color(String input) {
		return ChatColor.translateAlternateColorCodes('&', input);
	}

}
