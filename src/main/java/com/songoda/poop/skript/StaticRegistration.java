package com.songoda.poop.skript;

import org.bukkit.entity.Player;
import org.eclipse.jdt.annotation.Nullable;

import com.songoda.poop.events.FartEvent;
import com.songoda.poop.events.PoopEvent;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;

public class StaticRegistration {

	static {
		// Farting
		Skript.registerEvent("Fart", SimpleEvent.class, FartEvent.class, "[player] fart[ing]")
				.description("Called when a player farts.")
				.examples("on player farting:",
						"\tmessage \"You nasty!\"");
		EventValues.registerEventValue(FartEvent.class, Player.class, new Getter<Player, FartEvent>() {
			@Override
			@Nullable
			public Player get(FartEvent event) {
				return event.getPlayer();
			}
		}, 0);
		
		// Pooping
		Skript.registerEvent("Poop", SimpleEvent.class, PoopEvent.class, "[player] (shit[ting]|poop[ing]) [[their] pants]")
				.description("Called when a player poops from farting too much.")
				.examples("on player pooping their pants:",
						"\tmessage \"&lOh no, well time to get some new pants!\"");
		EventValues.registerEventValue(PoopEvent.class, Player.class, new Getter<Player, PoopEvent>() {
			@Override
			@Nullable
			public Player get(PoopEvent event) {
				return event.getPlayer();
			}
		}, 0);
	}

}
